# Length of sea trout returns

___


This repository contains the scripts and data used for the making of the following pre-print: https://biorxiv.org/cgi/content/short/2023.11.21.568009v1 

___
## REFERENCES

Josset Quentin, Beaulaton, Laurent, Romakkaniemi Atso & Marie Nevoux (2023). Changes in length-at-first return of a sea trout (Salmo trutta) population in northern France. bioRxiv. https://doi.org/10.1101/2023.11.21.568009

___
## STRUCTURE

length-of-sea-trout-returns/ # folder containing data and scripts
	- README.md # this file
	- AUTHORS # names of the authors
	- LICENSE.md # license under which these files and data may be utilised
  |scripts/ # R script to run the analysis and produce the plots  
  |data/ # data utilised in the scripts  
  
___
## ANALYSIS

Analysis are done with R 4.0.3, which needs to be installed before going any further. 
Following packages also needs to be installed and loaded before: 
> install.packages(c("tidyverse", "boot"))

___
## DONNEES

Josset, Q., Beaulaton, L., Romakkaniemi, A., & Nevoux, M. (2023). Data for « Changes in length-at-first return of a sea trout (Salmo trutta) population in northern France » (Version V1) [jeu de données]. Recherche Data Gouv. https://doi.org/10.57745/FPHLBT


___
## LICENSE
The content of this project itself is licensed under the [GNU GPL v3.0 license](https://www.gnu.org/licenses/gpl-3.0.en.html), except the datasets which are under the [Etalab Open License 2.0](https://spdx.org/licenses/etalab-2.0.html).